# IRRLICHT SKELETON #

Potential dabblings with Irrlicht. Currently just a room and an outdoor space with FPS movement and collisions.

This project uses the following third party libraries:

- Irrlicht
- OpenAL Soft

In addition Irrlicht uses code from the Independent JPEG Group, the zlib, and libpng. All of the licenses and the libraries they apply to can be found in the file COPYING-3RD-PARTY.

GUI elements, fonts and shaders are borrowed from the Irrlicht examples.
