Target := IrrlichtSkeleton
Sources := $(wildcard src/*.cpp) $(wildcard src/*/*.cpp)
BinPath = ./bin/$(SYSTEM)
ARCH := $(shell getconf LONG_BIT)

USERCPPFLAGS = 
USERCXXFLAGS = -O3 -ffast-math -std=c++14 -g -Wall -Wextra
USERLDFLAGS = -s

CPPFLAGS = -isystem ./include/Irrlicht -I./src -I./src/Entities -I./src/Globals -I./src/Guis -I./src/Worlds -I./include/AL -I./include -I/usr/X11R6/include $(USERCPPFLAGS)
CXXFLAGS = $(USERCXXFLAGS)
LDFLAGS = $(USERLDFLAGS)

all: all_linux

all_linux all_win32: LDFLAGS += -L./lib/Irrlicht/$(SYSTEM)/$(ARCH) -Wl,-R./lib/Irrlicht/$(SYSTEM)/$(ARCH) -lIrrlicht -L./lib/AL/$(SYSTEM)/$(ARCH) -Wl,-R./lib/AL/$(SYSTEM)/$(ARCH) -L./lib

all_linux: LDFLAGS += -L/usr/X11R6/lib$(LIBSELECT) -lGL -lXxf86vm -lXext -lX11 -lXcursor -ldl -lopenal
all_linux clean_linux: SYSTEM=Linux
all_win32 clean_win32: SYSTEM=Win32-gcc
all_win32 clean_win32: SUF=.exe
all_win32: CPPFLAGS += -D_IRR_STATIC_LIB_
all_win32: LDFLAGS += -lgdi32 -lwinspool -lcomdlg32 -lole32 -loleaut32 -luuid -lodbc32 -lodbccp32 -lwinmm -L"C:/Program Files (x86)/Microsoft DirectX SDK (June 2010)/Lib/x64" -ld3dx9d -ldxguid -ldinput8 -static -lopengl32 -lOpenAL32 
DESTPATH = $(BinPath)/$(Target)$(SUF)

all_linux all_win32:
	$(warning Building...)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(Sources) -o $(DESTPATH) $(LDFLAGS)

clean: clean_linux clean_win32
	$(warning Cleaning...)

clean_linux clean_win32:
	@$(RM) $(DESTPATH)

.PHONY: all all_win32 clean clean_linux clean_win32

ifeq ($(HOSTTYPE), x86_64)
LIBSELECT=64
endif

ifeq ($(HOSTTYPE), sun4)
LDFLAGS += -lrt
endif

