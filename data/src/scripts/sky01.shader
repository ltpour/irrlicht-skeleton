textures/skeleton/sky01
{
    qer_editorimage textures/skeleton/sky01_up.jpg //path of the image that shows up in Radiant
    skyparms textures/sky01 1024 - //farbox cloudheight nearbox
    q3map_sunExt 1 0.86 0.67 350 -102 40 2 16 //red green blue intensity degrees elevation deviance samples
    q3map_lightmapFilterRadius 0 160 //self other
    q3map_skyLight 300 3 //amount iterations
    surfaceparm    sky //tells the game it's a sky
    surfaceparm     noimpact //no projectiles impact here
    surfaceparm   nolightmap //no shadows on this surface
    surfaceparm     nomarks //no burn marks
}
