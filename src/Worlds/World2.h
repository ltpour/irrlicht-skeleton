#ifndef WORLD2_H
#define WORLD2_H

#include "irrlicht.h"

#include "Gui.h"
#include "World.h"

class Gui;

class World2 : public World {
public:
    World2();
    void update(irr::f32);
};

#endif
