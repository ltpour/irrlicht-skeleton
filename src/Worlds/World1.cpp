#include <iostream>
#include <memory>
#include <vector>

#include "irrlicht.h"

#include "Audio.h"
#include "Config.h"
#include "FPS.h"
#include "Game.h"
#include "Gui1.h"
#include "Highlighter.h"
#include "Sphere.h"
#include "Room.h"
#include "World1.h"
#include "World2.h"

using namespace irr;

World1::World1()
{
    gui = std::make_unique<Gui1>(this);
    irr::IrrlichtDevice* device = Game->getDevice();
    irr::scene::ISceneManager* smgr = device->getSceneManager();

    // add FPS camera
    std::vector<irr::SKeyMap> keyMap = Config.getKeyMap();
    camera = smgr->addCameraSceneNodeFPS(0, 500.0f, .3f, ID_IsNotPickable, keyMap.data(), keyMap.size(), false, 3.0f); // 500 = mouse sensitivity, 3 = jump speed
    camera->setPosition(core::vector3df(130,0,-100));
    camera->setTarget(core::vector3df(-90,0,-100));

    // hide mouse
    device->getCursorControl()->setVisible(false);

    // add entities to world
    entities.insert(std::make_unique<Room>(this));
    entities.insert(std::make_unique<Sphere>(this));
    entities.insert(std::make_unique<FPS>(this));
    entities.insert(std::make_unique<Highlighter>(this));

    // lighting for unselected nodes
    scene::ILightSceneNode * light = smgr->addLightSceneNode(0, core::vector3df(-60,100,400),
							     video::SColorf(1.0f,1.0f,1.0f,1.0f), 600.0f);
    light->setID(ID_IsNotPickable);
}

void World1::update(f32 deltaTime)
{
    irr::IrrlichtDevice* device = Game->getDevice();
    irr::video::IVideoDriver* driver = device->getVideoDriver();
    irr::scene::ISceneManager* smgr = device->getSceneManager();

    if (device->isWindowActive()) {
		driver->beginScene(true, true, 0);

		smgr->drawAll();

		for(auto& e : entities) {
			e->update(deltaTime);
		}

		gui->update(deltaTime);

		Audio->update(camera);

		driver->endScene();

		// switch world
		if (nextWorld) {
			Audio->clear();
			smgr->clear();
			Game->pushWorld(std::make_shared<World2>());
		}
	} else device->yield();
}
