#ifndef WORLD1_H
#define WORLD1_H

#include "irrlicht.h"

#include "Gui.h"
#include "World.h"

class Gui;

class World1 : public World {
public:
    World1();
    void update(irr::f32);
private:
    std::unique_ptr<Gui> gui;
};

#endif
