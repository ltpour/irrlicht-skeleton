#ifndef WORLD_H
#define WORLD_H

#include <memory>
#include <unordered_set>

#include "irrlicht.h"

#include "Entity.h"
#include "Game.h"

enum {
    // scene node flags for ray selection
    ID_IsNotPickable = 0,
    ID_IsPickable = 1 << 0, // pickable but not highlightable
    ID_IsHighlightable = 1 << 1
};

class Entity;

class World {
public:
    virtual void update(irr::f32) = 0;
    irr::scene::ICameraSceneNode* getCamera() { return camera; };
    bool nextWorld = false;
protected:
    std::unordered_set<std::unique_ptr<Entity>> entities;
    irr::scene::ICameraSceneNode* camera;
};

#endif
