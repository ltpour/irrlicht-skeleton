#ifndef AUDIO_H
#define AUDIO_H

#include <unordered_map>
#include <unordered_set>

#include "al.h"
#include "alc.h"
#include "irrlicht.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

class Audio_ {
public:
    enum Waveform { SINE, SQUARE, TRIANGLE, SAW };
    bool init(const irr::scene::ICameraSceneNode* = nullptr);
    void update(const irr::scene::ICameraSceneNode*);
    void clear();
    bool shutdown();
    ALuint playSound(const irr::scene::ISceneNode*, const ALchar*);
    ALuint playSound(const irr::scene::ISceneNode*, const Waveform = SINE);
    void stopSound(ALuint);
private:
    static std::unordered_map<ALuint, const irr::scene::ISceneNode*> sources;
    static std::unordered_set<ALuint> buffers;
    void followCamera(const irr::scene::ICameraSceneNode*);
    ALuint generateWaveform(const Waveform);
    ALuint loadWAV(const ALchar*);
    ALuint attachBuffer(ALuint, const irr::scene::ISceneNode*);
};

extern Audio_* Audio;

#endif
