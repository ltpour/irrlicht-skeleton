#include <iostream>
#include <memory>
#include <stack>

#include "irrlicht.h"

#include "Audio.h"
#include "Config.h"
#include "Game.h"
#include "World1.h"
#include "World.h"

using namespace irr;

#ifdef _MSC_VER
#pragma comment(lib, "Irrlicht.lib")
#endif

std::stack<std::shared_ptr<World>> worlds;

Game_* Game = new Game_;

// units
// 1 quake unit = 1 inch
// 12 units = 1 foot
// 40 units = 1 meter
// 1 players = 6 feet
// 128 units = regular ceiling
// 160 units = high ceiling

bool Game_::init()
{
    Config.load();
    video::E_DRIVER_TYPE driver = Config.getDriver();	  
    device = createDevice(driver, core::dimension2d<u32>(640, 480), 16, false);
    if (!device)
	return false;
    device->getFileSystem()->addFileArchive("data/foo.pk3");
    then = device->getTimer()->getTime();
    Audio->init();
    pushWorld(std::make_shared<World1>());
    return true;
}

void Game_::run()
{
    while(device->run()) {
	if(device->isWindowActive()) {
	    if(!peekWorld()) continue;
	    const u32 now = device->getTimer()->getTime();
	    f32 deltaTime = (f32)(now - then) / 1000.f;
	    peekWorld()->update(deltaTime);
	    then = now;
	} else device->yield();
    }
}

void Game_::shutdown()
{
    while(!worlds.empty()) popWorld();
    Audio->shutdown();
    device->drop();
}

IrrlichtDevice* Game_::getDevice()
{
    return device;
}

void Game_::pushWorld(std::shared_ptr<World> world)
{
    worlds.push(world);
}


void Game_::popWorld()
{
    worlds.pop();
}

void Game_::changeWorld(std::shared_ptr<World> world)
{
    if(worlds.empty())
	popWorld();
    pushWorld(world);
}

std::shared_ptr<World> Game_::peekWorld()
{
    if(worlds.empty())
	return nullptr;
    return worlds.top();
}
