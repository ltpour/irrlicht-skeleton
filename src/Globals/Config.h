#ifndef CONFIG_H
#define CONFIG_H

#include <vector>

#include "irrlicht.h"

class Config_ {
public:
    Config_();
    void load();
    std::vector<irr::SKeyMap> getKeyMap() { return keyMap; };
    irr::video::E_DRIVER_TYPE getDriver() { return driver; };
private:
    static std::vector<irr::SKeyMap> keyMap;
    static irr::video::E_DRIVER_TYPE driver;
};

extern Config_ Config;

#endif

