#include <fstream>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include "al.h"
#include "alc.h"
#include "irrlicht.h"

#include "Audio.h"

using namespace irr;

Audio_* Audio = new Audio_;

std::unordered_map<ALuint, const scene::ISceneNode*> Audio_::sources ({});
std::unordered_set<ALuint> Audio_::buffers ({});

void Audio_::followCamera(const scene::ICameraSceneNode* camera) {
    if (camera) {
	ALfloat position[3] = {camera->getPosition().X, camera->getPosition().Y, -camera->getPosition().Z};
	ALfloat orientation[6] = {camera->getTarget().X - camera->getPosition().X,
				  camera->getTarget().Y - camera->getPosition().Y,
				  -(camera->getTarget().Z - camera->getPosition().Z),
				  camera->getUpVector().X,
				  camera->getUpVector().Y,
				  -camera->getUpVector().Z};
	alListenerfv(AL_POSITION, position);
	alListenerfv(AL_ORIENTATION, orientation);
    }
}

ALuint Audio_::generateWaveform(const Waveform wf)
{
    ALuint buf;
    alGenBuffers(1, &buf);

    const float freq = 440.f;
    const int seconds = 20;
    const int amp = 32760;
    const unsigned sample_rate = 22050;
    constexpr size_t buf_size = seconds * sample_rate;

    short samples[buf_size];
    switch(wf) {
    case SINE:
	for(size_t i=0; i<buf_size; ++i) {
	    samples[i] = amp * sin( (2.f*float(M_PI)*freq)/sample_rate * i );
	}
	break;
    case SQUARE: // CAN BE SIMPLIFIED
	for(size_t i=0; i<buf_size; ++i) {
	    samples[i] = (sin( (2.f*float(M_PI)*freq)/sample_rate * i ) > 0) ? amp : -amp;
	}
	break;
    case TRIANGLE: // TODO
    case SAW: // TODO
    default:
	for(size_t i=0; i<buf_size; ++i) {
	    samples[i] = 0;
	}
    }

    alBufferData(buf, AL_FORMAT_MONO16, samples, buf_size, sample_rate);
    return buf;
}

ALuint Audio_::loadWAV(const ALchar* filename)
{
    ALuint buf = AL_NONE;
    int alFormat = AL_NONE;

    //std::string ChunkID(4, ' ');
    unsigned char ChunkID[4];
    unsigned char Format[4];
    unsigned char SubChunk1ID[4];
    unsigned int SubChunk1Size;
    unsigned short AudioFormat;
    unsigned short NumChannels;
    unsigned int SampleRate;
    unsigned short BitsPerSample;
    unsigned int SubChunk2Location;
    unsigned char SubChunk2ID[4];
    unsigned int SubChunk2Size;

    std::ifstream is (filename, std::ifstream::binary);

    do {
	if (!is) {
	    std::cerr << "could not open: " << filename << std::endl;
	    break;
	}

	is.read(reinterpret_cast<char *>(&ChunkID), sizeof(unsigned char) * 4);
	if (ChunkID[0] != 'R' || ChunkID[1] != 'I' || ChunkID[2] != 'F' || ChunkID[3] != 'F') {
	    std::cerr << ChunkID << "not RIFF" << std::endl;
	    break;
	}

	is.seekg (8, is.beg);
	is.read(reinterpret_cast<char *>(&Format), sizeof(unsigned char) * 4);
	if (Format[0] != 'W' || Format[1] != 'A' || Format[2] != 'V' || Format[3] != 'E') {
	    std::cerr << Format << "not WAVE" << std::endl;
	    break;
	}

	is.read(reinterpret_cast<char *>(&SubChunk1ID), sizeof(unsigned char) * 4);
	if (SubChunk1ID[0] != 'f' || SubChunk1ID[1] != 'm' || SubChunk1ID[2] != 't' || SubChunk1ID[3] != ' ') {
	    std::cerr << "not fmt" << std::endl;
	    break;
	}

	is.read(reinterpret_cast<char *>(&SubChunk1Size), sizeof(unsigned int));
	SubChunk2Location = (unsigned int)is.tellg() + SubChunk1Size;
	is.read(reinterpret_cast<char *>(&AudioFormat), sizeof(unsigned short));
	if (AudioFormat != 1) {
	    std::cerr << "non-PCM audio" << std::endl;
	    break;
	}

	is.read(reinterpret_cast<char *>(&NumChannels), sizeof(unsigned short));
	is.read(reinterpret_cast<char *>(&SampleRate), sizeof(unsigned int));
	is.seekg(34, is.beg);
	is.read(reinterpret_cast<char *>(&BitsPerSample), sizeof(unsigned short));

	if (NumChannels == 1 && BitsPerSample == 8) {
	    alFormat = AL_FORMAT_MONO8;
	} else if (NumChannels == 1 && BitsPerSample == 16) {
	    alFormat = AL_FORMAT_MONO16;
	} else if (NumChannels == 2 && BitsPerSample == 8) {
	    alFormat = AL_FORMAT_STEREO8;
	} else if (NumChannels == 2 && BitsPerSample == 16) {
	    alFormat = AL_FORMAT_STEREO16;
	} else {
	    alFormat = AL_INVALID;
	    std::cerr << "unsupported audio format" << std::endl;
	    return AL_NONE;
	}

	is.seekg(SubChunk2Location, is.beg);
	is.read(reinterpret_cast<char *>(&SubChunk2ID), sizeof(unsigned char) * 4);
	if (SubChunk2ID[0] != 'd' || SubChunk2ID[1] != 'a' || SubChunk2ID[2] != 't' || SubChunk2ID[3] != 'a') {
	    std::cerr << "missing audio data" << std::endl;
	    break;
	}

	is.read(reinterpret_cast<char *>(&SubChunk2Size), sizeof(unsigned int));
	alGenBuffers(1, &buf);
    } while(0);

    if (buf) {
	unsigned char Data[SubChunk2Size];
	is.read(reinterpret_cast<char *>(&Data), sizeof(unsigned char) * SubChunk2Size);
	alBufferData(buf, alFormat, Data, SubChunk2Size, SampleRate);
    }

    is.close();
    return buf;
}

bool Audio_::init(const scene::ICameraSceneNode* camera)
{
    ALCdevice* device = alcOpenDevice(nullptr);
    if (!device) {
	return false;
    }
    ALCcontext* context = alcCreateContext(device, nullptr);
    if (!context) {
	alcCloseDevice(device);
	return false;
    }
    alcMakeContextCurrent(context);
    alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);
    alGetError();
    if (camera)
	followCamera(camera);
    return true;
}

void Audio_::update(const scene::ICameraSceneNode* camera) {
    for(auto src = sources.begin(); src != sources.end(); ) {
	ALuint source = src->first;
	ALenum state;

	alGetSourcei(source, AL_SOURCE_STATE, &state);

	if(state != AL_PLAYING) {
	    // LEAVES UNUSED BUFFERS BEHIND(?)
	    //ALuint buffer;
	    //alGetSourcei(source, AL_BUFFER, &buffer);
	    alDeleteSources(1, &source);
	    //alDeleteBuffers(1, &buffer);
	    src = sources.erase(src);
	    //buffers.erase(buffer);
	    std::cout << "Removed source." << std::endl;
	} else {
	    // follow node
	    const scene::ISceneNode* node = src->second;
	    if (node) { // source attached to a node
		ALfloat position[3] = {node->getPosition().X, node->getPosition().Y, -node->getPosition().Z};
		alSourcefv(source, AL_POSITION, position);
	    }
	    // REMEMBER TO STOP AUDIO BEFORE DESTROYING THE NODE IT'S ATTACHED TO
	    ++src;
	}
    }
    followCamera(camera);
}

void Audio_::clear()
{
    for(auto src = sources.begin(); src != sources.end(); ) {
	ALuint source = src->first;
	alDeleteSources(1, &source);
	src = sources.erase(src);
    }
    for(auto bfr = buffers.begin(); bfr != buffers.end(); ) {
	ALuint buffer = *bfr;
	alDeleteBuffers(1, &buffer);
	bfr = buffers.erase(bfr);
    }
}

bool Audio_::shutdown()
{
    clear();
    ALCcontext* context = alcGetCurrentContext();
    ALCdevice* device = alcGetContextsDevice(context);
    alcMakeContextCurrent(nullptr);
    alcDestroyContext(context);
    return alcCloseDevice(device);
}

ALuint Audio_::attachBuffer( ALuint buf, const scene::ISceneNode* node )
{
    ALuint src;
    ALCcontext* context = alcGetCurrentContext();
    ALCdevice* device = alcGetContextsDevice(context);

    alGenSources(1, &src);
    if(alGetError() != AL_NO_ERROR) {
	std::cerr << "Failed to create OpenAL source!" << std::endl;
	alcCloseDevice(device);
	return AL_NONE;
    }

    if(!buf) {
	std::cerr << "Could not load sound" << std::endl;
	alDeleteSources(1, &src);
	alcCloseDevice(device);
	return AL_NONE;
    }

    alSourcei(src, AL_BUFFER, buf);
    alSourcef(src, AL_MIN_GAIN, 0.0f);
    alSourcef(src, AL_MAX_GAIN, 1.0f);
    alSourcef(src, AL_REFERENCE_DISTANCE, 39.37f); // 1 meter = 39.37 inches
    alSourcef(src, AL_ROLLOFF_FACTOR, 1.0f);
    alSourcei(src, AL_LOOPING, false);
    if(node) {
	ALfloat position[3] = {node->getPosition().X, node->getPosition().Y, -node->getPosition().Z};
	alSourcefv(src, AL_POSITION, position);
    }

    alSourcePlay(src);

    sources.insert(std::make_pair(src, node));
    buffers.insert(buf);

    return src;
}

ALuint Audio_::playSound(const scene::ISceneNode *node, const ALchar *soundFile)
{
    return attachBuffer(loadWAV(soundFile), node);
}

ALuint Audio_::playSound(const irr::scene::ISceneNode *node, const Waveform wf)
{
    return attachBuffer(generateWaveform(wf), node);
}

void Audio_::stopSound( ALuint src)
{
    alSourceStop(src);
}
