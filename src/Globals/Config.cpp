#include <vector>

#include "irrlicht.h"

#include "Config.h"

using namespace irr;

Config_ Config;

std::vector<irr::SKeyMap> Config_::keyMap {
    {EKA_MOVE_FORWARD, KEY_UP},
    {EKA_MOVE_FORWARD, KEY_KEY_W},
    {EKA_MOVE_BACKWARD, KEY_DOWN},
    {EKA_MOVE_BACKWARD, KEY_KEY_S},
    {EKA_STRAFE_LEFT, KEY_LEFT},
    {EKA_STRAFE_LEFT, KEY_KEY_A},
    {EKA_STRAFE_RIGHT, KEY_RIGHT},
    {EKA_STRAFE_RIGHT, KEY_KEY_D},
    {EKA_JUMP_UP, KEY_INSERT},
    {EKA_JUMP_UP, KEY_SPACE}};
video::E_DRIVER_TYPE Config_::driver;

// defaults
Config_::Config_()
{
    // #ifndef _WIN32
    driver = video::EDT_OPENGL;
    // #endif
    // #ifdef _WIN32
    // driver = video::EDT_DIRECT3D9;
    // #endif
}

void Config_::load()
{
}
