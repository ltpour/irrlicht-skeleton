#ifndef GAME_H
#define GAME_H

#include <memory>
#include <stack>

#include "irrlicht.h"

#include "World.h"

class World;

class Game_ {
public:
    bool init();
    void run();
    void shutdown();
    void pushWorld(std::shared_ptr<World>);
    void popWorld();
    void changeWorld(std::shared_ptr<World>);
    std::shared_ptr<World> peekWorld();
    irr::IrrlichtDevice* getDevice();
private:
    irr::IrrlichtDevice* device;
    std::stack<std::shared_ptr<World>> worlds;
    irr::u32 then; // for delta time
};

extern Game_* Game;

#endif

