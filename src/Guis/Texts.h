#ifndef TEXTS_H
#define TEXTS_H

#include "irrlicht.h"

#include "GuiElement.h"
#include "Gui.h"

class Texts : public GuiElement {
public:
    Texts(Gui* const);
    void update(irr::f32);
};

#endif
