#ifndef GUI1_H
#define GUI1_H

#include <unordered_set>

#include "irrlicht.h"

#include "Gui.h"

class Gui1 : public Gui {
public:
    Gui1(World* const);
    void update(irr::f32);
};

#endif
