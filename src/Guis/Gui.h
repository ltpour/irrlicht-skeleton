#ifndef GUI_H
#define GUI_H

#include <memory>
#include <unordered_set>
#include <vector>

#include "irrlicht.h"

#include "GuiElement.h"
#include "Game.h"

class GuiElement;

class Gui {
public:
    virtual void update(irr::f32) = 0;
    virtual const irr::video::ITexture* getImages() { return images; };
    virtual const std::vector<irr::gui::IGUIFont*> getFonts() { return fonts; };
protected:
    std::vector<std::unordered_set<std::unique_ptr<GuiElement>>> guiElementLayers;
    std::vector<irr::gui::IGUIFont*> fonts;
    World* world;
    irr::video::ITexture* images;
};

#endif
