#include <iostream>
#include <memory>
#include <unordered_set>
#include <vector>

#include "irrlicht.h"

#include "Dungeon.h"
#include "Game.h"
#include "Gui1.h"
#include "Imp.h"
#include "Logo.h"
#include "Texts.h"

using namespace irr;

Gui1::Gui1(World* const world)
{
    this->world = world;

    irr::IrrlichtDevice* device = Game->getDevice();
    irr::video::IVideoDriver* driver = device->getVideoDriver();

    images = driver->getTexture("data/2ddemo.png");
    driver->makeColorKeyTexture(images, core::position2d<s32>(0,0));
    
    driver->getMaterial2D().TextureLayer[0].BilinearFilter=true;
    driver->getMaterial2D().AntiAliasing=video::EAAM_FULL_BASIC;

    fonts.push_back(device->getGUIEnvironment()->getBuiltInFont());
    fonts.push_back(device->getGUIEnvironment()->getFont("data/fonthaettenschweiler.bmp"));

    std::unordered_set<std::unique_ptr<GuiElement>> guiElementLayer1;
    guiElementLayer1.insert(std::make_unique<Dungeon>(this));
    guiElementLayers.push_back(std::move(guiElementLayer1));

    std::unordered_set<std::unique_ptr<GuiElement>> guiElementLayer2;
    guiElementLayer2.insert(std::make_unique<Imp>(this));
    guiElementLayers.push_back(std::move(guiElementLayer2));

    std::unordered_set<std::unique_ptr<GuiElement>> guiElementLayer3;
    guiElementLayer3.insert(std::make_unique<Logo>(this));
    guiElementLayer3.insert(std::make_unique<Texts>(this));
    guiElementLayers.push_back(std::move(guiElementLayer3));
}

void Gui1::update(f32 deltaTime)
{
    for(auto& l : guiElementLayers) {
	for(auto& e : l) {
	    e->update(deltaTime);
	}
    }
}
