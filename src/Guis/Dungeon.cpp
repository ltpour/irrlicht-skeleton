#include "irrlicht.h"

#include "Game.h"
#include "Dungeon.h"

using namespace irr;

Dungeon::Dungeon(Gui* const gui)
{
    this->gui = gui;
}

void Dungeon::update(f32)
{
    irr::IrrlichtDevice* device = Game->getDevice();
    irr::video::IVideoDriver* driver = device->getVideoDriver();

    const video::ITexture* images = gui->getImages();

    // draw fire & dragons background world
    driver->draw2DImage(images, core::position2d<s32>(50,50),
			core::rect<s32>(0,0,342,224), 0,
			video::SColor(255,255,255,255), true);

}
