#ifndef IMP_H
#define IMP_H

#include "irrlicht.h"

#include "GuiElement.h"
#include "Gui.h"

class Imp : public GuiElement {
public:
    Imp(Gui* const);
    void update(irr::f32);
};

#endif
