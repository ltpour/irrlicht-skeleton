#include "irrlicht.h"

#include "Game.h"
#include "Logo.h"

using namespace irr;

Logo::Logo(Gui* const gui)
{
    this->gui = gui;
}

void Logo::update(f32)
{
    irr::IrrlichtDevice* device = Game->getDevice();
    irr::video::IVideoDriver* driver = device->getVideoDriver();

    const video::ITexture* images = gui->getImages();

    driver->enableMaterial2D();
    driver->draw2DImage(images, core::rect<s32>(10,10,108,48),
			core::rect<s32>(354,87,442,118));
    driver->enableMaterial2D(false);

    core::position2d<s32> m = device->getCursorControl()->getPosition();
    driver->draw2DRectangle(video::SColor(100,255,255,255),
			    core::rect<s32>(m.X-20, m.Y-20, m.X+20, m.Y+20));

    //u32 time = device->getTimer()->getTime();
}
