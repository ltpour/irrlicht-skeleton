#ifndef LOGO_H
#define LOGO_H

#include "irrlicht.h"

#include "GuiElement.h"
#include "Gui.h"

class Logo : public GuiElement {
public:
    Logo(Gui* const);
    void update(irr::f32);
};

#endif
