#include "irrlicht.h"

#include "Game.h"
#include "Imp.h"

using namespace irr;

Imp::Imp(Gui* const gui)
{
    this->gui = gui;
}

void Imp::update(f32)
{
    irr::IrrlichtDevice* device = Game->getDevice();
    irr::video::IVideoDriver* driver = device->getVideoDriver();

    const video::ITexture* images = gui->getImages();

    u32 time = device->getTimer()->getTime();

    core::rect<s32> imp1(349,15,385,78);
    core::rect<s32> imp2(387,15,423,78);
    
    // draw flying imp
    driver->draw2DImage(images, core::position2d<s32>(164,125),
			(time/500 % 2) ? imp1 : imp2, 0,
			video::SColor(255,255,255,255), true);

    // draw second flying imp with colorcylce
    driver->draw2DImage(images, core::position2d<s32>(270,105),
			(time/500 % 2) ? imp1 : imp2, 0,
			video::SColor(255,(time) % 255,255,255), true);
}
