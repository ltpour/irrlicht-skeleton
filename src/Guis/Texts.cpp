#include "irrlicht.h"

#include "Game.h"
#include "Texts.h"

using namespace irr;

Texts::Texts(Gui* const gui)
{
    this->gui = gui;
}

void Texts::update(f32)
{
    irr::IrrlichtDevice* device = Game->getDevice();

    const std::vector<irr::gui::IGUIFont*> fonts = gui->getFonts();

    u32 time = device->getTimer()->getTime();

    // draw some text
    if (fonts.at(0))
	fonts.at(0)->draw(L"This demo shows that Irrlicht is also capable of drawing 2D graphics.",
		    core::rect<s32>(130,10,300,50),
		    video::SColor(255,255,255,255));

    // draw some other text
    if (fonts.at(1))
	fonts.at(1)->draw(L"Also mixing with 3d graphics is possible.",
                    core::rect<s32>(130,20,300,60),
                    video::SColor(255,time % 255,time % 255,255));
}
