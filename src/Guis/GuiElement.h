#ifndef GUI_ELEMENT_H
#define GUI_ELEMENT_H

#include "irrlicht.h"

#include "Gui.h"

class Gui;

class GuiElement {
public:
    virtual void update(irr::f32) = 0;
protected:
    Gui* gui;
};

#endif
