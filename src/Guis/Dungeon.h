#ifndef DUNGEON_H
#define DUNGEON_H

#include "irrlicht.h"

#include "GuiElement.h"
#include "Gui.h"

class Dungeon : public GuiElement {
public:
    Dungeon(Gui* const);
    void update(irr::f32);
};

#endif
