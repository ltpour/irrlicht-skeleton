#include "irrlicht.h"

#include "FPS.h"
#include "Game.h"

using namespace irr;

FPS::FPS(World* const world)
{
    this->world = world;
}

void FPS::update(f32)
{
    IrrlichtDevice* device = Game->getDevice();
    video::IVideoDriver* driver = device->getVideoDriver();
    int newFps = driver->getFPS();
    if (fps != newFps) {
	fps = newFps;
	core::stringw str = L"Irrlicht Skeleton - FPS:";
	str += fps;
	device->setWindowCaption(str.c_str());
    }
}
