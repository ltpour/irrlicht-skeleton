#ifndef FPS_H
#define FPS_H

#include "irrlicht.h"

#include "Entity.h"
#include "World.h"

class FPS : public Entity {
public:
    FPS(World* const);
    void update(irr::f32);
private:
    int fps = -1;
};

#endif
