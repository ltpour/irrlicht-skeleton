#include "irrlicht.h"

#include "Audio.h"
#include "Cube.h"
#include "Game.h"
#include "World.h"

using namespace irr;

Cube::Cube(World* const world)
{ 
    this->world = world;
    IrrlichtDevice* device = Game->getDevice();
    video::IVideoDriver* driver = device->getVideoDriver();
    scene::ISceneManager* smgr = device->getSceneManager();
    video::SMaterial material;

    node = static_cast<scene::IAnimatedMeshSceneNode*>
	(smgr->addAnimatedMeshSceneNode(smgr->getMesh("cube.md3"), 0, ID_IsPickable | ID_IsHighlightable));
    node->setPosition(core::vector3df(-90,-100,-140));
    node->setRotation(core::vector3df(0,90,0));
    material.setTexture(0, driver->getTexture("cube01.jpg"));
    material.Lighting = true;
    material.NormalizeNormals = true;
    node->getMaterial(0) = material;
    scene::ITriangleSelector* selector = smgr->createTriangleSelector(static_cast<scene::IAnimatedMeshSceneNode*>(node));
    node->setTriangleSelector(selector);
    selector->drop();

    Audio->playSound(node, Audio->SQUARE); // attach sound source to cube
}

void Cube::update(f32 deltaTime)
{
    scene::ISceneNode* camera = world->getCamera();
    moveTowards(camera, 40, deltaTime);
    if (node->getAbsolutePosition().getDistanceFrom(camera->getAbsolutePosition()) < 10)
	world->nextWorld = true;
}
