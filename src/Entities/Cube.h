#ifndef CUBE_H
#define CUBE_H

#include "irrlicht.h"

#include "Entity.h"
#include "World.h"

class Cube : public Entity {
public:
    Cube(){};
    Cube(World* const);
    void update(irr::f32);
};

#endif
