#ifndef ROOM_H
#define ROOM_H

#include "irrlicht.h"

#include "Entity.h"
#include "World.h"

class Room : public Entity {
public:
    Room(World* const);
    void update(irr::f32){};
};

#endif
