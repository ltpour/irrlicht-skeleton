#include <cmath>

#include "irrlicht.h"

#include "Game.h"
#include "Entity.h"

using namespace irr;

void Entity::moveTowards(scene::ISceneNode* object, f32 speed, f32 deltaTime)
{
    core::vector3df objectPosition = object->getPosition();
    core::vector3df subjectPosition = node->getPosition();
    f32 xDir = subjectPosition.X - objectPosition.X;
    f32 yDir = subjectPosition.Y - objectPosition.Y;
    f32 zDir = subjectPosition.Z - objectPosition.Z;
    f32 hyp = sqrt(xDir * xDir + yDir * yDir + zDir * zDir);
    xDir /= hyp;
    yDir /= hyp;
    zDir /= hyp;
    core::vector3df position = {subjectPosition.X - xDir * speed * deltaTime, subjectPosition.Y - yDir * speed * deltaTime, subjectPosition.Z - zDir * speed * deltaTime};
    node->setPosition(position);
}

