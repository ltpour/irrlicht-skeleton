#include "irrlicht.h"

#include "Audio.h"
#include "Game.h"
#include "Sphere.h"
#include "World.h"

using namespace irr;

Sphere::Sphere(World* const world)
{
    this->world = world;
    IrrlichtDevice* device = Game->getDevice();
    video::IVideoDriver* driver = device->getVideoDriver();
    scene::ISceneManager* smgr = device->getSceneManager();
    video::SMaterial material;
 
    node = static_cast<scene::IAnimatedMeshSceneNode*>
	(smgr->addAnimatedMeshSceneNode(smgr->getMesh("sphere.md3"), 0, ID_IsPickable | ID_IsHighlightable));
    node->setPosition(core::vector3df(-90,-100,-80));
    node->setRotation(core::vector3df(0,90,0));
    material.setTexture(0, driver->getTexture("sphere01.jpg"));
    material.Lighting = true;
    material.NormalizeNormals = true;
    node->getMaterial(0) = material;
    scene::ITriangleSelector* selector = smgr->createTriangleSelector(static_cast<scene::IAnimatedMeshSceneNode*>(node));
    node->setTriangleSelector(selector);
    selector->drop();

    Audio->playSound(node, "data/test.wav"); // attach sound source to sphere
}

void Sphere::update(f32 deltaTime)
{
    scene::ISceneNode* camera = world->getCamera();
    moveTowards(camera, 40, deltaTime);
    if (node->getAbsolutePosition().getDistanceFrom(camera->getAbsolutePosition()) < 10)
	world->nextWorld = true;
}
