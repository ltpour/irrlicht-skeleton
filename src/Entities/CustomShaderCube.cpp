#include "irrlicht.h"

#include "Config.h"
#include "Game.h"
#include "CustomShaderCube.h"
#include "World.h"

using namespace irr;

class CustomShaderCallBack : public video::IShaderConstantSetCallBack
{
public:
    virtual void OnSetConstants(video::IMaterialRendererServices* services, s32) {
	IrrlichtDevice* device = Game->getDevice();
	video::IVideoDriver* driver = device->getVideoDriver();
	core::matrix4 invWorld = driver->getTransform(video::ETS_WORLD);
	invWorld.makeInverse();

	services->setVertexShaderConstant(invWorld.pointer(), 0, 4);

	core::matrix4 worldViewProj;
	worldViewProj = driver->getTransform(video::ETS_PROJECTION);
	worldViewProj *= driver->getTransform(video::ETS_VIEW);
	worldViewProj *= driver->getTransform(video::ETS_WORLD);

	services->setVertexShaderConstant(worldViewProj.pointer(), 4, 4);

	core::vector3df pos = device->getSceneManager()->
	    getActiveCamera()->getAbsolutePosition();

	services->setVertexShaderConstant(reinterpret_cast<f32*>(&pos), 8, 1);

	video::SColorf col(0.0f,1.0f,1.0f,0.0f);

	services->setVertexShaderConstant(reinterpret_cast<f32*>(&col), 9, 1);

	core::matrix4 world = driver->getTransform(video::ETS_WORLD);
	world = world.getTransposed();

	services->setVertexShaderConstant(world.pointer(), 10, 4);
    }
};

CustomShaderCube::CustomShaderCube(World* const world) : Cube(world)
{ 
    IrrlichtDevice* device = Game->getDevice();
    video::IVideoDriver* driver = device->getVideoDriver();

    io::path vsFileName; // filename for the vertex shader
    io::path psFileName; // filename for the pixel shader

    video::E_DRIVER_TYPE driverType = driver->getDriverType();
    switch(driverType)
    {
    case video::EDT_OPENGL:
	psFileName = "data/opengl.psh";
	vsFileName = "data/opengl.vsh";
	break;
    case video::EDT_DIRECT3D9: // disable shaders
	psFileName = "";
	vsFileName = "";
	break;
    default: // disable shaders
	psFileName = "";
	vsFileName = "";
    }

    if (!driver->queryFeature(video::EVDF_PIXEL_SHADER_1_1) &&
	!driver->queryFeature(video::EVDF_ARB_FRAGMENT_PROGRAM_1)) {
	device->getLogger()->log("WARNING: Pixel shaders disabled "\
				 "because of missing driver/hardware support.");
	psFileName = "";
    }

    if (!driver->queryFeature(video::EVDF_VERTEX_SHADER_1_1) &&
	!driver->queryFeature(video::EVDF_ARB_VERTEX_PROGRAM_1)) {
	device->getLogger()->log("WARNING: Vertex shaders disabled "\
				 "because of missing driver/hardware support.");
	vsFileName = "";
    }

    video::IGPUProgrammingServices* gpu = driver->getGPUProgrammingServices();
    s32 newMaterialType = 0;

    if (gpu) {
	CustomShaderCallBack* mc = new CustomShaderCallBack();

	newMaterialType = gpu->addShaderMaterialFromFiles(
	    vsFileName, psFileName, mc, video::EMT_SOLID);

	mc->drop();
    }

    node->setMaterialFlag(video::EMF_LIGHTING, false);
    node->setMaterialType((video::E_MATERIAL_TYPE)newMaterialType);
}
