#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include "irrlicht.h"

#include "Entity.h"
#include "World.h"

class Highlighter : public Entity {
public:
    Highlighter(World* const);
    void update(irr::f32);
private:
    irr::scene::ISceneNode* highlightedSceneNode;
    irr::scene::IBillboardSceneNode* bill;
};

#endif
