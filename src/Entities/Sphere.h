#ifndef SPHERE_H
#define SPHERE_H

#include "irrlicht.h"

#include "Entity.h"
#include "World.h"

class Sphere : public Entity {
public:
    Sphere(World* const);
    void update(irr::f32);
};

#endif
