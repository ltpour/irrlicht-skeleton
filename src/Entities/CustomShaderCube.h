#ifndef CUSTOMSHADERCUBE_H
#define CUSTOMSHADERCUBE_H

#include "irrlicht.h"

#include "Cube.h"
#include "World.h"

class CustomShaderCube : public Cube {
public:
    CustomShaderCube(World* const);
};

#endif
