#include "irrlicht.h"

#include "Highlighter.h"
#include "Game.h"

using namespace irr;

Highlighter::Highlighter(World* const world)
{
    this->world = world;
    IrrlichtDevice* device = Game->getDevice();
    video::IVideoDriver* driver = device->getVideoDriver();
    scene::ISceneManager* smgr = device->getSceneManager();

    // add a billboard to show what we're looking at
    bill = smgr->addBillboardSceneNode();
    bill->setMaterialType(video::EMT_TRANSPARENT_ADD_COLOR );
    bill->setMaterialTexture(0, driver->getTexture("particle.bmp"));
    bill->setMaterialFlag(video::EMF_LIGHTING, false);
    bill->setMaterialFlag(video::EMF_ZBUFFER, false);
    bill->setSize(core::dimension2d<f32>(20.0f, 20.0f));
    bill->setID(ID_IsNotPickable);

    // remember which scene node is highlighted
    highlightedSceneNode = 0;
}

void Highlighter::update(f32)
{
    IrrlichtDevice* device = Game->getDevice();
    video::IVideoDriver* driver = device->getVideoDriver();
    scene::ISceneManager* smgr = device->getSceneManager();
    scene::ICameraSceneNode* camera = world->getCamera();

    // unlight any currently highlighted scene node
    if (highlightedSceneNode) {
	highlightedSceneNode->setMaterialFlag(video::EMF_LIGHTING, true);
	highlightedSceneNode = 0;
    }

    // ray cast out from the camera to a distance of 1000
    core::line3d<f32> ray;
    ray.start = camera->getPosition();
    ray.end = ray.start + (camera->getTarget() - ray.start).normalize() * 1000.0f;

    // find the nearest collision point/triangle
    core::vector3df intersection;
    core::triangle3df hitTriangle;
    scene::ISceneCollisionManager* collMan = smgr->getSceneCollisionManager();
    scene::ISceneNode * selectedSceneNode =
	collMan->getSceneNodeAndCollisionPointFromRay(ray, intersection, hitTriangle, ID_IsPickable, 0);

    // move billboard to collision position and draw hit triangle
    if(selectedSceneNode) {
	bill->setPosition(intersection);

	// material for selection triangle
	irr::video::SMaterial material;
	material.Wireframe=true;

	// reset transform and render
	driver->setTransform(video::ETS_WORLD, core::matrix4());
	driver->setMaterial(material);
	driver->draw3DTriangle(hitTriangle, video::SColor(0,255,0,0));

	// highlight hit scene node if highlightable
	if((selectedSceneNode->getID() & ID_IsHighlightable) == ID_IsHighlightable) {
	    highlightedSceneNode = selectedSceneNode;

	    // draw node with full brightness
	    highlightedSceneNode->setMaterialFlag(video::EMF_LIGHTING, false);
	}
    }
}
