#include "irrlicht.h"

#include "TransparentCube.h"
#include "World.h"

using namespace irr;

TransparentCube::TransparentCube(World* const world) : Cube(world)
{ 
    node->setMaterialType(video::EMT_TRANSPARENT_ADD_COLOR);
}
