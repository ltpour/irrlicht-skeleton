#include "irrlicht.h"

#include "Audio.h"
#include "Game.h"
#include "BigRoom.h"

using namespace irr;

BigRoom::BigRoom(World* const world)
{
    this->world = world;
    IrrlichtDevice* device = Game->getDevice();
    scene::ISceneManager* smgr = device->getSceneManager();

    // load map and add selector
    scene::IAnimatedMesh* q3levelmesh = smgr->getMesh("bigroom.bsp");
    scene::IMeshSceneNode* q3node = 0;
    if (q3levelmesh)
	q3node = smgr->addOctreeSceneNode(q3levelmesh->getMesh(0), 0, ID_IsPickable);
    scene::ITriangleSelector* selector = 0;
    if (q3node) {
	q3node->setPosition(core::vector3df(-1350,-130,-1400));
	selector = smgr->createOctreeTriangleSelector(q3node->getMesh(), q3node, 128);
	q3node->setTriangleSelector(selector);
    }

    // add collision animator
    irr::scene::ICameraSceneNode* camera = world->getCamera();
    if (selector) {
	scene::ISceneNodeAnimator* anim =
	    smgr->createCollisionResponseAnimator(selector, camera, core::vector3df(30,50,30),
						  core::vector3df(0,-10,0), core::vector3df(0,30,0));
	selector->drop();
	camera->addAnimator(anim);
	anim->drop();
    }
}
