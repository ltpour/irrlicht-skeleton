#ifndef BIGROOM_H
#define BIGROOM_H

#include "irrlicht.h"

#include "Entity.h"
#include "World.h"

class BigRoom : public Entity {
public:
    BigRoom(World* const);
    void update(irr::f32){};
};

#endif
