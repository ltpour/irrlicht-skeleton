#ifndef OUTDOOR_H
#define OUTDOOR_H

#include "irrlicht.h"

#include "Entity.h"
#include "World.h"

class Outdoor : public Entity {
public:
    Outdoor(World* const);
    void update(irr::f32){};
};

#endif
