#ifndef TRANSPARENTCUBE_H
#define TRANSPARENTCUBE_H

#include "irrlicht.h"

#include "Cube.h"
#include "World.h"

class TransparentCube : public Cube {
public:
    TransparentCube(World* const);
};

#endif
