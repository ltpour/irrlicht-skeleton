#ifndef ENTITY_H
#define ENTITY_H

#include "irrlicht.h"

#include "World.h"

class World;

class Entity {
public:
    virtual void update(irr::f32) = 0;
    void moveTowards(irr::scene::ISceneNode* object, irr::f32 speed, irr::f32 deltaTime);
protected:
    irr::scene::ISceneNode* node;
    World* world;
};

#endif
