#include "Game.h"

int main()
{
    if(Game->init()) {
	Game->run();
    	Game->shutdown();
    }
    return 0;
}
